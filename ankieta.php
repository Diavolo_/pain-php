<?php
$servername = "localhost";
$username = "root";
$password = "";
$dbname = "ankieta1";

$conn = new mysqli($servername, $username, $password, $dbname);


if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}

$sql = "SELECT id, pytanie FROM pytania";
$result = $conn->query($sql);


if ($_SERVER["REQUEST_METHOD"] == "POST") {

    foreach ($_POST['odpowiedzi'] as $id => $odpowiedz) {
        $id = (int)$id;
        $odpowiedz = $conn->real_escape_string($odpowiedz);
        
        
            $sql_insert = "INSERT INTO odpowiedzi_user (id, odp) VALUES (?, ?)";
            $stmt = $conn->prepare($sql_insert);
            $stmt->bind_param("is", $id, $odpowiedz);
            $stmt->execute();
            $stmt->close();
        
    }
    echo "Odpowiedzi zapisane.";
    header("location: " . $_SERVER['PHP_SELF']);
}

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Formularz</title>
    <style>
        #form{
            text-align: center;
            width: 300px;
            height: 450px;
            border: 10px solid black;
           
        
        }
        #button{
            width: 200px;
            height: 60px;
            background-color: brown;
            border: 10px solid blanchedalmond;
            color: aliceblue;
        }

    </style>
</head>
<body>

<form id="form"method="post" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>">
    <?php
    if ($result->num_rows > 0) {
        while ($row = $result->fetch_assoc()) {
            echo "<p>{$row['pytanie']}:</p>";
            
            $sql_options = "SELECT id, odpowiedz FROM odpowiedzi WHERE id = {$row['id']}";
            $result_options = $conn->query($sql_options);
            
            if ($result_options->num_rows > 0) {
                while ($row_option = $result_options->fetch_assoc()) {
                    echo "<input type='radio' name='odpowiedzi[{$row['id']}]' value='{$row_option['odpowiedz']}' required>{$row_option['odpowiedz']}<br>";
                }
            } else {
                echo "Brak dostępnych odpowiedzi.";
            }
            
            echo "<br>";
        }
    } else {
        echo "Brak pytań w bazie danych.";
    }
    ?>
    <input type="submit" value="Zapisz odpowiedzi" id="button">
</form>

</body>
</html>

<?php
$conn->close();
?>
